// DOM elements
const main = document.querySelector('main');
const voicesSelect = document.getElementById('voices');
const textarea = document.getElementById('text');
const readBtn = document.getElementById('read');
const toggleBtn = document.getElementById('toggle');
const closeBtn = document.getElementById('close');

const data = [
    {
        image: './img/alejandro-contreras-wTPp323zAEw-unsplash.jpg',
        text: "Flamingo without his legs sorry"
    },
    {
        image: './img/alessandro-desantis-9_9hzZVjV8s-unsplash.jpg',
        text: "Golden hour elephant"
    },
    {
        image: './img/andy-holmes-D6TqIa-tWRY-unsplash.jpg',
        text: "Giraffe without his long neck"
    },
    {
        image: './img/gary-bendig-6GMq7AGxNbE-unsplash.jpg',
        text: "It is a baby raccoon"
    },
    {
        image: './img/geran-de-klerk-wYy3rvvgjAU-unsplash.jpg',
        text: "Jaguar on a rock"
    },
    {
        image: './img/gwen-weustink-I3C1sSXj1i8-unsplash.jpg',
        text: "Another Jaguar because they are pretty"
    },
    {
        image: './img/jonatan-pie-xgTMSz6kegE-unsplash.jpg',
        text: "Cute little snow fox"
    },
    {
        image: './img/kar-ming-moo-Q_3WmguWgYg-unsplash.jpg',
        text: "White tiger looking better than most of us"
    },
    {
        image: './img/kevin-mueller-aeNg4YA41P8-unsplash.jpg',
        text: "Bird from the amazon"
    },
    {
        image: './img/laura-college-K_Na5gCmh38-unsplash.jpg',
        text: "Deer with murder weapons"
    },
    {
        image: './img/maurits-bausenhart-QMRN_GX7p4I-unsplash.jpg',
        text: "Scar but as a normal lion"
    },
    {
        image: './img/ningyu-he-K22SK31G-QM-unsplash.jpg',
        text: "Round panda"
    },
    {
        image: './img/ray-hennessy-xUUZcpQlqpM-unsplash.jpg',
        text: "Fox in the snow"
    },
    {
        image: './img/thomas-oldenburger-1SQFd9_zNW4-unsplash.jpg',
        text: "Bitch I am a..."
    },
    {
        image: './img/wexor-tmg-L-2p8fapOA8-unsplash.jpg',
        text: "Sea turtle like in Nemo"
    },
];

data.forEach(createBox);

// Create speech boxes
function createBox(item) {
    const box = document.createElement('div');

    const { image, text } = item;

    box.classList.add('box');
    box.innerHTML = `
        <img src="${image}" alt="${text}" />
        <p class="info">${text}</p>
    `;

    // Add speak event
    box.addEventListener('click', () => {
        setTextMessage(text);
        speakText();

        // Add active effect
        box.classList.add('active');
        setTimeout(() => box.classList.remove('active'), 800);
    });

    main.appendChild(box);
}

// Init speech synth
const message = new SpeechSynthesisUtterance();

// Store voices
let voices = [];

function getVoices() {
    voices = speechSynthesis.getVoices();

    voices.forEach(voice => {
        const option = document.createElement('option');

        option.value = voice.name;
        option.innerText = `${voice.name} ${voice.lang}`;

        voicesSelect.appendChild(option);
    });
}

// Set text
function setTextMessage(text) {
    message.text = text;
}

// Speak text
function speakText() {
    speechSynthesis.speak(message);
}

// Set voice 
function setVoice(e) {
    message.voice = voices.find(voice => voice.name === e.target.value);
}

// Voices changed
speechSynthesis.addEventListener('voiceschanged', getVoices);

// Toggle text box
toggleBtn.addEventListener('click', () => 
    document.getElementById('text-box').classList.toggle('show')
);

// Close text box
closeBtn.addEventListener('click', () => 
    document.getElementById('text-box').classList.remove('show')
);

// Change voice
voicesSelect.addEventListener('change', setVoice)

// Read text box
readBtn.addEventListener('click', () => {
    setTextMessage(textarea.value);
    speakText();
});

getVoices();

